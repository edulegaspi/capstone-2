const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Product name is required."]
	},
	description: {
		type: String,
		required: [true, "Description is required."]
	},
	color: {
		type: String,
		required: [true, "Size is required."]
	},
	size: {
		type: String,
		required: [true, "Size is required."]
	},
	price: {
		type: Number,
		required: [true, "Price is required."]
	},
	quantity: {
		type: Number,
		required: [true, "Quantity is required."]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdDate: {
			type: Date,
			default: new Date()
	},
	orderedByUser: [
		{
			userId: {
				type: String,
				required: [true, "UserId is required."]
			},
			orderedOn: {
				type: Date,
				defualt: new Date()
			}
		}
	]
})

module.exports = mongoose.model("Product", productSchema)