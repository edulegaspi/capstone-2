const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"] 
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile number is required"]
	},
	orders: [
		{
			orderId: {
				type: String,
			},
			products: [
			{
				productId: {
					type: String,
					// required: [true, "Product ID is required"]
				},
				quantity: {
					type: Number,
					// required: [true, "Quantity is required"]	
				}
			}],
			createdDate: {
				type: Date,
				default: new Date()
			},
			totalAmount: {
				type: Number,
				// required: [true, "Total Amount is required"]
			},
			shippingAddress: {
				region: {
					type: String,
					// required: [true, "Region is required"]
				},
				city: {
					type: String,
					// required: [true, "City is required"]
				},
				district: {
					type: String,
					// required: [true, "District is required"]
				},
				street: {
					type: String,
					// required: [true, "Street is required"]
				},
				unitNo_bldgName: {
					type: String,
					// required: [true, "Unit Number and Building Name are required"]
				}
			}
		}
	]
})


module.exports = mongoose.model("User", userSchema);