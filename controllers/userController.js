const User = require('../models/user');
const Order = require('../models/order');
const Product = require('../models/product');
const auth = require('../auth');
const bcrypt = require('bcrypt');


//register new user
module.exports.registerUser = (body) => {
	let newUser = new User({
		firstName: body.firstName,
		lastName: body.lastName,
		email: body.email,
		mobileNo: body.mobileNo,
		password: bcrypt.hashSync(body.password, 10)
	})

	return newUser.save().then((user, error) => {
		if (error) {
			return false;
		}else {
			return true;	
		}
	})
}


//user login
module.exports.loginUser = (body) => {
	return User.findOne({email: body.email}).then(result => {
		if (result === null) {
			return false
		}else {
			const isPasswordCorrect = bcrypt.compareSync(body.password, result.password)

			if (isPasswordCorrect) {
				return {acess: auth.createAccessToken(result.toObject())}
			}else {
				return false
			}
		}
	})
}


//getting user details
module.exports.getProfile = (userId) => {
	return User.findById(userId).then(result => {
		result.password = undefined; //hiding the password for security purpose
		return result;
	})
}


//updating a user's permission to admin
module.exports.updateUserPermission = (params) => {
	let updatedUserPermission = {
		isAdmin: true
	}

	return User.findByIdAndUpdate(params.userId, updatedUserPermission).then((
		user, error) => {
		if (error) {
			return false
		}else {
			return true
		}
	})
}


//placing an order
module.exports.newOrder = async (data) => {
	console.log(data)
	let productSaveStatus = await Product.findById(data.productId).then(product => {
			product.orderedByUser.push({
				userId: data.userId})

		return product.save().then((order, err) => {
			if (err) {
				return false;
			}else {
				return true;
			}
		})	
	})

	let userSaveStatus = await User.findById(data.userId).then(user => {
			user.orders.push({
				productId: data.productId,
				quantity: data.quantity,
				totalAmount: data.totalAmount
			})
		
		return user.save().then((user, err) => {
			if (err) {
				return false;
			}else {
				return true;
			}
		})	 
	})

	if (productSaveStatus && userSaveStatus) {
		return true;
	}else {
		return false;
	}
}


//retrieving all orders of a user
module.exports.getAllOrders = (data) => {
	return User.findById(data.id).then(result => {
		return result.orders
	})
}



