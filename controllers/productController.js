const Product = require('../models/product')
const User = require('../models/user');


//creating a new product
module.exports.addProduct = (body) => {

	let newProduct = new Product ({
		name: body.name,
		description: body.description,
		color: body.color,
		size: body.size,
		price: body.price,
		quantity: body.quantity
	})

	return newProduct.save().then((product, error) => {
		if (error) {
			return false;
		}else {
			return true;
		}

	})
}


//retrieving all active products
module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then((result, error) => {
		if (error) {
			return false;
		}else {
			return result;
		}
	})	
}


//rerieving a specific product
module.exports.getProduct = (product) => {
	return Product.findById(product.productId).then((result, error) => {
		return result;
	})	
}


//updating a product's information
module.exports.updateProduct = (params, body) => {
	let updatedProduct = {
		name: body.name,
		description: body.description,
		color: body.color,
		size: body.size,
		price: body.price,
		quantity: body.quantity
	}

	return Product.findByIdAndUpdate(params.productId, updatedProduct).then((
		product, error) => {
		if (error) {
			return false
		}else {
			return true
		}
	})
}


//archiving a specific product
module.exports.archiveProduct = (params) => {
	let updatedProduct = {
		isActive: false
	}

	return Product.findByIdAndUpdate(params.productId, updatedProduct).then((
		product, error) => {
		if (error) {
			return false
		}else {
			return true
		}
	})
}
