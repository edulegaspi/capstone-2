const express = require('express');
const router = express.Router();
const auth = require('../auth');
const productController = require('../controllers/productController')


//route to create a new product
router.post('/new', auth.verify, (req, res) => {

	let token = auth.decode(req.headers.authorization)

	// console.log(token)

	if (token.isAdmin === true) {
		productController.addProduct(req.body).then(result => res.send(result))	
	}else {
		res.send({auth: 'not an admin'})
	}
})


//route to retrieve all active products
router.get('/all', (req, res) => {
	productController.getAllActive().then(result => res.send(result))
})


//route to retrieve a specific product
router.get('/:productId', (req, res) => {
	productController.getProduct(req.params).then(result => res.send(result))
})


//route to update a product's information
router.put('/:productId', auth.verify, (req, res) => {

	let token = auth.decode(req.headers.authorization)

	if (token.isAdmin === true) {
		productController.updateProduct(req.params, req.body).then(result => res.send(result))
	}else {
		res.send({auth: 'not an admin'})
	}
})


//route to archive a product
router.delete('/:productId', auth.verify, (req, res) => {

	let token = auth.decode(req.headers.authorization)

	if (token.isAdmin === true) {
		productController.archiveProduct(req.params).then(result => res.send(result))
	}else {
		res.send({auth: 'not an admin'})
	}
})



module.exports = router;