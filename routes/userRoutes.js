const express = require('express');
const router = express.Router();
const auth = require('../auth');
const userController = require('../controllers/userController');
const orderController = require('../controllers/orderController');


//route to register a new user
router.post('/sign_up', (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result))
})


//route to authenticate user
router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result))
})


//route for getting user details
router.get('/details', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	console.log(userData)

	userController.getProfile(userData.id).then(result => res.send(result))
})


//route to make a user an admin
router.put('/:userId', auth.verify, (req, res) => {

	let token = auth.decode(req.headers.authorization)

	if (token.isAdmin === true) {
		userController.updateUserPermission(req.params).then(result => res.send(result))
	}else {
		res.send({auth: 'not an admin'})
	}
})



//route for placing an order
router.post('/order/new', auth.verify, (req, res) => {
	const orderData = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		quantity: req.body.quantity,
	}

	userController.newOrder(orderData).then(result => res.send(result))
})


//route to get all orders of authenticated user
router.get('/orders', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	console.log(userData)
	userController.getAllOrders(userData).then(result => res.send(result))
})


module.exports = router;